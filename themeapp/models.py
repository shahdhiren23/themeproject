from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="category", null=True, blank=True)

    def __str__(self):
        return self.title


class Food(models.Model):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, null=True, blank=True)
    image = models.ImageField(upload_to="Food")
    description = models.TextField()
    price = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):

        return self.name

# class slider


class Slider(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='slider')

    def __str__(self):
        return self.title

# class testimonial


class Testimonial(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='testimonial')
    organization = models.CharField(max_length=200)
    review = models.TextField()

    def __str__(self):
        return self.name


# class gallery
class Gallery(models.Model):
    image = models.ImageField(upload_to='gallery')
    caption = models.CharField(max_length=200)

    def __str__(self):
        return self.caption
