from django.shortcuts import render, redirect
from django.views.generic import *
from .models import*
from .forms import*
from django.contrib.auth import authenticate, login, logout


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(seelf, **kwargs):
        context = super().get_context_data(**kwargs)
        context["myfoods"] = Food.objects.all()
        context["mytestimonials"] = Testimonial.objects.all()
        context["mygallerys"] = Gallery.objects.all()
        context["mysliders"] = Slider.objects.all()
        context["mycategorys"] = Category.objects.all()

        return context


class AboutView(TemplateView):
    template_name = "about.html"


class ContactView(TemplateView):
    template_name = "contact.html"


class GalleryView(TemplateView):
    template_name = "gallery.html"

    def get_context_data(seelf, **kwargs):
        context = super().get_context_data(**kwargs)
        context["mygallerys"] = Gallery.objects.all()

        return context


class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("/login/")

        return super().dispatch(request, *args, **kwargs)


class AdminHomeView(AdminRequiredMixin, TemplateView):
    template_name = "admintemplates/adminhome.html"


class AdminFoodListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/adminfoodlist.html"
    queryset = Food.objects.all()
    context_object_name = 'foodlist'


class AdminFoodCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/adminfoodcreate.html"
    form_class = FoodForm
    success_url = '/firm-admin/food/list/'


class AdminFoodUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/adminfoodcreate.html"
    form_class = FoodForm
    model = Food
    success_url = '/firm-admin/food/list/'


class AdminFoodDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/adminfooddelete.html"
    model = Food
    success_url = '/firm-admin/food/list/'


class AdminCategoryCreateView(AdminRequiredMixin, CreateView):
    template_name = "admintemplates/adminfoodcategory.html"
    form_class = CategoryForm
    success_url = '/firm-admin/food/categorylist/'


class AdminCategoryListView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/admincategorylist.html"
    queryset = Category.objects.all()
    context_object_name = "allcategory"


class AdminFoodUpdateView(AdminRequiredMixin, UpdateView):
    template_name = "admintemplates/adminfoodcategory.html"
    model = Category
    form_class = CategoryForm
    success_url = '/firm-admin/category/list/'


class AdminFoodDeleteView(AdminRequiredMixin, DeleteView):
    template_name = "admintemplates/adminfoodcategorydelete.html"
    model = Category
    success_url = '/firm-admin/category/list/'


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = "/firm-admin"

    def form_valid(self, form):
        a = form.cleaned_data["username"]
        b = form.cleaned_data.get("password")
        user = authenticate(username=a, password=b)
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, self.template_name,
                          {"error": "Invalid Credentials", 'form': form})

        return super().form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)

        return redirect("/")
