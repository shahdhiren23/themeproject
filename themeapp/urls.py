from django.urls import path
from .views import *

app_name = "themeapp"
urlpatterns = [
    path("", HomeView.as_view(), name='home'),
    path("gallery/", GalleryView.as_view(), name='gallery'),
    path("about/", AboutView.as_view(), name='about'),
    path("contact/", ContactView.as_view(), name='contact'),
    path("firm-admin/", AdminHomeView.as_view(), name='adminhome'),

    # food urls
    path("firm-admin/food/list/", AdminFoodListView.as_view(), name='adminfoodlist'),
    path("firm-admin/food/add/", AdminFoodCreateView.as_view(),
         name='adminfoodcreate'),
    path("firm-admin/food/<int:pk>/update/",
         AdminFoodUpdateView.as_view(), name='adminfoodupdate'),
    path("firm-admin/food/<int:pk>/delete/",
         AdminFoodDeleteView.as_view(), name='adminfooddelete'),

    # Category urls
    path("firm-admin/category/create/",
         AdminCategoryCreateView.as_view(), name='admincategorycreate'),
    path('firm-admin/category/list/',
         AdminCategoryListView.as_view(), name="admincategorylist"),
    path("firm-admin/category/<int:pk>/update/",
         AdminFoodUpdateView.as_view(), name='admincategoryupdate'),
    path("firm-admin/category/<int:pk>/delete/",
         AdminFoodDeleteView.as_view(), name='admincategorydelete'),

    path("login/", LoginView.as_view(), name='login'),
    path("logout/", LogoutView.as_view(), name='logout'),


]
